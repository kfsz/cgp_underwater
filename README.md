# CGP Project - Underwater World  
  
## Screenshots  
  
<table align="center">
    <tr>
        <td align="center"><img src="screenshots/1.png?raw=true" alt="SS1"></td>
        <td align="center"><img src="screenshots/2.png?raw=true" alt="SS2"></td>
    </tr>
    <tr>
        <td align="center"><img src="screenshots/5.png?raw=true" alt="SS5"></td>
        <td align="center"><img src="screenshots/7.png?raw=true" alt="SS7"></td>
    </tr>
    <tr>
        <td align="center"><img src="screenshots/3.png?raw=true" alt="SS3"></td>
        <td align="center"><img src="screenshots/10.png?raw=true" alt="SS10"></td>
    </tr>
</table>
  
## Features  
  
 - 3d movement, with use of quaternions
 - two movement modes
 - first and third person camera
 - over 10 levels of different water depth
 - sky level
 - two skyspheres and one skybox
 - BRDF spaceship, with possibility of selection from over 100 different metallic/roughness combinations
 - environmental mapped portals
 - main fish swarm
 - fish particle system (selectable number of swarms)
 - fishes cast shadows over other fishes (shadow mapping)
 - flashlight
 - different possible colors of flashlight's light
 - disco mode, with randomized flashlight's colors
 - lots of transparent bubbles
 - possible bubble collision, which results in bubble destruction
     
### Special feature

 - submarine spaceship glow
 - trail of glowing particles (+ particle movement)
 - 4 diffrent particle textures
 - particle mode changes depending on ship's movement
