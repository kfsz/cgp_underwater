#include "glew.h"
#include "freeglut.h"
#include "glm.hpp"
#include "ext.hpp"
#include "math.h"
#include <iostream>
#include <vector>
#include <windows.h>

#include "Shader_Loader.h"
#include "Render_Utils.h"
#include "Camera.h"
#include "Texture.h"

#include "common.h"

void changeLevel(bool direction);

void changeCamera();

void checkBounds();

void kaboom();

void goBack();

void checkBoundsNoChange(float max);

void randomizeFlashLightColor();

glm::vec3 cohension(glm::vec3 currentFishPosition, int batch);

glm::vec3 separation(glm::vec3 currentFishPosition, glm::vec3 currentFishVelocity, int batch);

glm::vec3 alignment(glm::vec3 currentFishVelocity, int batch);

void goToSky();
