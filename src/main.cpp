#include "glew.h"
#include "freeglut.h"
#include "glm.hpp"
#include "ext.hpp"
#include "math.h"
#include <iostream>
#include <vector>
#include <windows.h>

#include "Shader_Loader.h"
#include "Render_Utils.h"
#include "Camera.h"
#include "Texture.h"

#include "common.h"
#include "draw.h"
#include "misc.h"
#include "camera_extension.h"
#include "particles.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

/*
todo:
- add some kind of glow to ship when deep underwater <- done
- add some kind of particle system trailing after our ship <- done
- (optional) add gaussian blur <- pass
- make particles always move up <- done

notes:
- most of these shouldn't be globals probably
but I can't be bothered to check (and fix) which ones

current state:
- half-term project finished
- project finished!
*/

GLuint programColor;
GLuint programTexture;
GLuint programLightning;

Core::Shader_Loader shaderLoader;

obj::Model shipModel;
obj::Model sphereModel;
obj::Model fishModel;

glm::vec3 cameraPos = glm::vec3(0, 0, 5);
glm::vec3 cameraDir;
glm::vec3 cameraSide;

bool rotate = false;
bool firstPerson = true;
glm::vec3 shipPos = glm::vec3(0, -0.25f, 0.5f);

glm::vec4 defBackgroundColor = glm::vec4(0.0f, 0.1f, 0.2f, 1.0f);
glm::vec4 backgroundColor = glm::vec4(0.0f, 0.1f, 0.2f, 1.0f);

glm::vec3 flashLightColorRandom = glm::vec3(0.6f, 0.2f, 0.6f);

struct level Level;

float cameraAngle = 0;

std::vector<glm::vec3> BubblePositions;
std::vector<glm::vec3> FishPositions;
std::vector<float> FishSize;

glm::mat4 cameraMatrix, perspectiveMatrix;

glm::vec3 lightDir = glm::normalize(glm::vec3(0.4f, -1.0f, 0.005f));
glm::quat rotation = glm::quat(1, 0, 0, 0);

GLuint textureBubble;
GLuint textureFish;
GLuint textureBG;
GLuint textureGround;
GLuint textureShip;

float speedBonus = 0.0f;
float fishVeritcalDirection = 0.3f;
unsigned long count = 0;

float cameraY, cameraX, lastY, lastX;
bool firstMouse = true;
bool tour = false;
bool flashlight = false;
bool flashlightColor = false;
bool discoMode = false;

// for tour mode
long tick = 0;
//int SCREEN_WIDTH = 1024; int SCREEN_HEIGHT = 800;
int SCREEN_WIDTH = 1920; int SCREEN_HEIGHT = 1080;
bool FULLSCREEN = 1;

// cgp additions

// fish swarm
int FishSwarmCount = 3;
int FishInSwarmCount = 15;
std::vector<glm::vec3> FishSwarmPositions;
std::vector<glm::vec3> FishSwarmVelocity;

// shadow mapping
GLuint programShadow;
GLuint programDepth;
GLuint depthTexture;
GLuint FramebufferObject;
GLuint programLightningShadow;

// brdf
GLuint programShadowBRDF;
GLuint programLightningShadowBRDF;
glm::vec3 lightPos = glm::vec3(0.4f, 150.0f, 0.2f);

// normal mapping
GLuint programShadowNormal;
GLuint programLightningShadowNormal;
std::vector<float> tangent(1119);
GLuint textureEarth;
GLuint textureEarthNormal;
GLuint textureFishNormal;

// em
GLuint programTextureEM;
GLuint programLightningEM;
GLuint programSimpleEM;
GLuint textureAsteroid;
std::vector<glm::vec4> portals;

// lightning
glm::mat4 projectionMatrix;
glm::mat4 cameraMatrix2;
glm::mat4 VPMatrix;

// vba/vao model drawing
int Index = 0;
GLuint vao;
unsigned int fishVAO, fishVBO, fishEBO;

// other
glm::vec3 shipPosOffset = cameraDir * 1.5;
bool sky = false;

float shipMetallic = 0.2f;
float shipRoughness = 0.015f;

// skybox
GLuint skybox;
GLuint cubemapTexture;
GLuint programSkybox;

// new in v.3

// ship glow
GLuint programShip;
GLuint textureShipGlow;

// particles
Particles *particles, *particles_right;
glm::vec3 cameraPosPrev = cameraPos;
glm::vec3 cameraUp;

// better movement
float accelerate = 0, strafe = 0;
bool w_pressed = false, s_pressed = false, a_pressed = false, d_pressed;

void movement();

const float cubeVertices[] = {
	30.5f, 30.5f, 30.5f, 1.0f,
	30.5f, -30.5f, 30.5f, 1.0f,
	-30.5f, 30.5f, 30.5f, 1.0f,

	30.5f, -30.5f, 30.5f, 1.0f,
	-30.5f, -30.5f, 30.5f, 1.0f,
	-30.5f, 30.5f, 30.5f, 1.0f,

	30.5f, 30.5f, -30.5f, 1.0f,
	-30.5f, 30.5f, -30.5f, 1.0f,
	30.5f, -30.5f, -30.5f, 1.0f,

	30.5f, -30.5f, -30.5f, 1.0f,
	-30.5f, 30.5f, -30.5f, 1.0f,
	-30.5f, -30.5f, -30.5f, 1.0f,

	-30.5f, 30.5f, 30.5f, 1.0f,
	-30.5f, -30.5f, 30.5f, 1.0f,
	-30.5f, -30.5f, -30.5f, 1.0f,

	-30.5f, 30.5f, 30.5f, 1.0f,
	-30.5f, -30.5f, -30.5f, 1.0f,
	-30.5f, 30.5f, -30.5f, 1.0f,

	30.5f, 30.5f, 30.5f, 1.0f,
	30.5f, -30.5f, -30.5f, 1.0f,
	30.5f, -30.5f, 30.5f, 1.0f,

	30.5f, 30.5f, 30.5f, 1.0f,
	30.5f, 30.5f, -30.5f, 1.0f,
	30.5f, -30.5f, -30.5f, 1.0f,

	30.5f, 30.5f, -30.5f, 1.0f,
	30.5f, 30.5f, 30.5f, 1.0f,
	-30.5f, 30.5f, 30.5f, 1.0f,

	30.5f, 30.5f, -30.5f, 1.0f,
	-30.5f, 30.5f, 30.5f, 1.0f,
	-30.5f, 30.5f, -30.5f, 1.0f,

	30.5f, -30.5f, -30.5f, 1.0f,
	-30.5f, -30.5f, 30.5f, 1.0f,
	30.5f, -30.5f, 30.5f, 1.0f,

	30.5f, -30.5f, -30.5f, 1.0f,
	-30.5f, -30.5f, -30.5f, 1.0f,
	-30.5f, -30.5f, 30.5f, 1.0f,
};

		  //\\
		 //||\\
		//_||_\\
	   ///||||\\\
	  //O//||\\O\\
	 ///////\\\\\\\
	/////| my |\\\\\
   /////submarine\\\\
  ///// spaceship \\\\

void drawObjectShip(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, float metallic, float roughness, bool transparency = false, bool skipFlashlight = false)
{
	GLuint program;

	if (flashlight == 1 && !skipFlashlight)
		// let's leave it like this for testing
		program = programLightningShadowBRDF;
	else
		program = programShip;

	glUseProgram(program);

	glUniform1f(glGetUniformLocation(program, "metallic"), metallic);
	glUniform1f(glGetUniformLocation(program, "roughness"), roughness);

	glm::mat4 concatenatedMatrix = VPMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "lightMVP"), 1, GL_FALSE, (float*)&concatenatedMatrix);

	glUniform3f(glGetUniformLocation(program, "cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform1f(glGetUniformLocation(program, "power"), Level.lightPower);
	//std::cout << Level.lightPower << std::endl;

	if (transparency == false)
		glUniform1f(glGetUniformLocation(program, "transPower"), 1.0f);
	else
		glUniform1f(glGetUniformLocation(program, "transPower"), 0.15f); // 0.3 is also fine

	glUniform3f(glGetUniformLocation(program, "lightPos"), lightPos.x, lightPos.y, lightPos.z);

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);

	Core::SetActiveTexture(textureId, "textureSampler", program, 0);
	Core::SetActiveTexture(depthTexture, "textureDepth", program, 1);
	Core::SetActiveTexture(textureShipGlow, "textureGlow", program, 2);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;

	// this thing probably shouldn't be in draw functions...
	float xx, yy, zz = 1.0f;
	if (discoMode == 1) {
		flashlight = 1;
		xx = round((std::rand() % 10) + 1) / 10;
		yy = round((std::rand() % 10) + 1) / 10;
		zz = round((std::rand() % 10) + 1) / 10;
	}

	if (flashlight == 1) {

		glUniform3f(glGetUniformLocation(program, "lightPosition"), cameraPos.x + shipPosOffset.x, cameraPos.y + shipPosOffset.y, cameraPos.z + shipPosOffset.z);

		glUniform3f(glGetUniformLocation(program, "lightDirection"), cameraDir.x, cameraDir.y, cameraDir.z);
		glUniform1f(glGetUniformLocation(program, "lightCutOff"), glm::cos(glm::radians(30.5f)));
		glUniform1f(glGetUniformLocation(program, "lightOuterCutOff"), glm::cos(glm::radians(37.5f))); //was 34.5

		if (flashlightColor == 1)
			glUniform3f(glGetUniformLocation(program, "lightColor"), flashLightColorRandom.x, flashLightColorRandom.y, flashLightColorRandom.z);
		else
			if (discoMode == 1)
				glUniform3f(glGetUniformLocation(program, "lightColor"), xx, yy, zz);
			else
				glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void renderSkyScene()
{
	// no shadows in the sky
	// gotta remove these that were here before though
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferObject);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w);

	// proper rendering starts here
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w);

	// render our ship
	glm::mat4 shipInitialTransformation = glm::translate(shipPos) * glm::rotate(glm::radians(180.0f), glm::vec3(0, 1, 0)) * glm::scale(glm::vec3(0.25f));
	glm::mat4 shipModelMatrix = glm::translate(cameraPos + cameraDir * 0.5f) * glm::mat4_cast(glm::inverse(rotation)) * shipInitialTransformation;
	//drawObjectShadowBRDF(&shipModel, shipModelMatrix, textureShip, shipMetallic, shipRoughness);
	drawObjectShip(&shipModel, shipModelMatrix, textureShip, shipMetallic, shipRoughness);

	// render garage
	float newShipMetallic, newShipRoughness;
	glm::mat4 planetModelMatrix;
	for (int column = 1; column <= 10; column++)
		for (int row = 1; row <= 10; row++) {
			glm::vec3 garageShipPosition = glm::vec3(-7.0f + 2 * row, -7.0f + 2 * column, -6.5f);
			planetModelMatrix = glm::translate(garageShipPosition);

			newShipMetallic = 0.1f * column;
			newShipRoughness = 0.1f * row;

			// don't display our own ship
			if (shipMetallic != newShipMetallic || shipRoughness != newShipRoughness)
				//drawObjectShadowBRDF(&shipModel, planetModelMatrix, textureShip, newShipMetallic, newShipRoughness);
				drawObjectShip(&shipModel, planetModelMatrix, textureShip, newShipMetallic, newShipRoughness);

			// ship changing
			int distance = 1;
			if (abs(garageShipPosition.x - cameraPos.x) < distance && abs(garageShipPosition.y - cameraPos.y) < distance && abs(garageShipPosition.z - cameraPos.z) < distance) {
				shipMetallic = newShipMetallic;
				shipRoughness = newShipRoughness;
				//std::cout << "changed ship!" << std::endl;
			}
		}

	// skybox
	drawSkybox(glm::scale(glm::translate(glm::vec3(0.0f, 0.0f, 0.0f)), glm::vec3(5.0f, 5.0f, 5.0f)), skybox);

	// portals with em
	for (int i = 0; i < portals.size(); i++)
	{
		glm::mat4 planetModelMatrix = glm::translate(glm::vec3(portals[i])) * glm::scale(glm::vec3(portals[i].w));
		drawSimpleEM(&sphereModel, planetModelMatrix, textureAsteroid);

		// using portal
		int distance = portals[i].w;
		if (abs(glm::vec3(portals[i]).x - cameraPos.x) < distance && abs(glm::vec3(portals[i]).y - cameraPos.y) < distance && abs(glm::vec3(portals[i]).z - cameraPos.z) < distance) {
			goToSky();
		}
	}

	glDepthMask(GL_FALSE);

	// particles
	if (!firstPerson) {
		particles->update();
		particles->draw();

		particles_right->update();
		particles_right->draw();
	}

	glDepthMask(GL_TRUE);

	// special checking for sky, due to smaller area
	checkBoundsNoChange(90.0f);

	glutSwapBuffers();
}

void renderScene()
{
	//main

	//movement
	movement();

	//matrix calculation
	cameraMatrix = createCameraMatrix();
	perspectiveMatrix = Core::createPerspectiveMatrix();

	// for proper flashlight calculation
	if (firstPerson)
		shipPosOffset = glm::vec3(0.1);
	else
		shipPosOffset = cameraDir * 1.5;

	// quick branch out
	if (sky) {
		renderSkyScene();
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferObject);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w);

	// create shadow map
	projectionMatrix = glm::ortho<float>(-20.0f, 20.0f, -20.0f, 20.0f, -20.0f, 350.0f);
	// maybe it needs to be farther out? -  * 158 / could use proper translation later on
	cameraMatrix2 = glm::lookAt(-lightDir * 158, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	VPMatrix = projectionMatrix * cameraMatrix2;

	// only ship and fishes are casting shadows
	glm::mat4 shipInitialTransformation = glm::translate(shipPos) * glm::rotate(glm::radians(180.0f), glm::vec3(0, 1, 0)) * glm::scale(glm::vec3(0.25f));
	glm::mat4 shipModelMatrix = glm::translate(cameraPos + cameraDir * 0.5f) * glm::mat4_cast(glm::inverse(rotation)) * shipInitialTransformation;

	createDepth(&shipModel, shipModelMatrix);

	for (int i = 0; i < FishPositions.size(); i++) {
		createDepth(&fishModel, glm::scale(glm::translate(FishPositions[i]), glm::vec3(FishSize[i], FishSize[i], FishSize[i])));
	}
	for (int i = 0; i < FishSwarmPositions.size(); i++) {
		createDepth(&fishModel, glm::translate(FishSwarmPositions[i]));
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//create main environment

	// render our ship
	drawObjectShip(&shipModel, shipModelMatrix, textureShip, shipMetallic, shipRoughness, false, true);

	count++;
	if (count % 30 == 0) {
		fishVeritcalDirection = -fishVeritcalDirection;
		count = 0;
	}

	//create fishes #1
	for (int i = 0; i < FishPositions.size(); i++) {

		drawObjectShadowNormal(&fishModel, glm::scale(glm::translate(FishPositions[i]), glm::vec3(FishSize[i], FishSize[i], FishSize[i])), textureFish, textureFishNormal);
		FishPositions[i] = FishPositions[i] + glm::vec3(0.0f, 0.0f + fishVeritcalDirection, 1.0f - FishSize[i] / 50.0f);
		if (FishPositions[i].z > 275) FishPositions[i].z = -275;

		//testing suite
		//drawObjectTexture(&fishModel, glm::scale(glm::translate(FishPositions[i]), glm::vec3(FishSize[i], FishSize[i], FishSize[i])), depthTexture);
	}

	//create fishes #2
	int current = 0, count = 0;
	float weight = 0.0025f;
	glm::vec3 currentFishVelocity;
	for (int i = 0; i < FishSwarmPositions.size(); i++) {
		// there probably exists a better way to do this
		// well, it's better than division, I guess...
		count++;
		if (count >= FishInSwarmCount) {
			count = 0;
			current++;
			if (current >= FishSwarmCount)
				current = 0;
		}

		currentFishVelocity = FishSwarmVelocity[i] * weight +
			cohension(FishSwarmPositions[i], current) * weight * 3 +
			separation(FishSwarmPositions[i], FishSwarmVelocity[i], current) * weight * 2 +
			alignment(FishSwarmVelocity[i], current) * weight +
			//slight value adjustment
			FishSwarmVelocity[i] * -0.003f +
			FishSwarmVelocity[i].x * -0.008f;

		FishSwarmPositions[i] += currentFishVelocity;
		drawObjectShadowNormal(&fishModel, glm::translate(FishSwarmPositions[i]), textureFish, textureFishNormal);
	}

	//fish swarms teleport
	for (int j = 0; j < FishSwarmCount; j++) {
		int test = 2 + j * FishInSwarmCount;
		if (FishSwarmPositions[test].x > 130) {
			glm::vec3 currentSwarmPosition = glm::ballRand(85.0);
			for (int i = 0 + j * FishInSwarmCount; i < FishInSwarmCount - 1 + j * FishInSwarmCount; i++) {
				//std::cout << "respawn x " << std::endl;
				glm::vec3 newFishPosition = glm::ballRand(15.0);
				FishSwarmPositions[i] = newFishPosition + currentSwarmPosition;
				FishSwarmVelocity[i] = glm::vec3(0.2f, 0.0f, 0.0f);
			}
		}
		if (FishSwarmPositions[test].y > 130) {
			glm::vec3 currentSwarmPosition = glm::ballRand(85.0);
			for (int i = 0 + j * FishInSwarmCount; i < FishInSwarmCount - 1 + j * FishInSwarmCount; i++) {
				//std::cout << "respawn y " << std::endl;
				glm::vec3 newFishPosition = glm::ballRand(15.0);
				FishSwarmPositions[i] = newFishPosition + currentSwarmPosition;
				FishSwarmVelocity[i] = glm::vec3(0.2f, 0.0f, 0.0f);
			}
		}
		if (FishSwarmPositions[test].z > 130) {
			glm::vec3 currentSwarmPosition = glm::ballRand(85.0);
			for (int i = 0 + j * FishInSwarmCount; i < FishInSwarmCount - 1 + j * FishInSwarmCount; i++) {
				//std::cout << "respawn z " << std::endl;
				glm::vec3 newFishPosition = glm::ballRand(15.0);
				FishSwarmPositions[i] = newFishPosition + currentSwarmPosition;
				FishSwarmVelocity[i] = glm::vec3(0.2f, 0.0f, 0.0f);
			}
		}
	}

	// skysphere
	if (Level.ground == false)
		drawObjectTexture(&sphereModel, glm::scale(glm::translate(glm::vec3(0.0f, 0.0f, 0.0f)), glm::vec3(285.0f, 285.0f, 285.0f)), textureBG, true);

	// ground(sphere?)
	else
		drawObjectTexture(&sphereModel, glm::scale(glm::translate(glm::vec3(0.0f, 0.0f, 0.0f)), glm::vec3(300.0f, 300.0f, 300.0f)), textureGround);

	// create bubbles
	glDepthMask(GL_FALSE);

	for (int i = 0; i < BubblePositions.size(); i++) {
		drawObjectTexture(&sphereModel, glm::translate(BubblePositions[i]), textureBubble, true);
		BubblePositions[i] = BubblePositions[i] + glm::vec3(0.0f, 0.4f, 0.0f);
		if (BubblePositions[i].y > 100) BubblePositions[i].y = -100;

		//collision detection
		int distance = 1;
		if (abs(BubblePositions[i].x - cameraPos.x) < distance && abs(BubblePositions[i].y - cameraPos.y) < distance && abs(BubblePositions[i].z - cameraPos.z) < distance) {
			kaboom();
			BubblePositions[i].y = -100;
		}
	}

	// gonna simply use billboarding
	// 2d particles going out of engines

	// create particles
	// show only when using 3rd person mode
	if (!firstPerson) {
		particles->update();
		particles->draw();

		particles_right->update();
		particles_right->draw();
	}

	glDepthMask(GL_TRUE);

	//tour mode
	if (tour == true) {

		cameraPos -= cameraSide * 0.5f;
		cameraX = 0.68f;
		checkBounds();

		tick++;

		// makes sure it always go smoothly around, with never going out of bounds
		if (abs(cameraPos.x - (-45.0)) < 1.0 && abs(cameraPos.z - (0.0)) < 1.0 && tick > 100) {
			cameraPos.x = -45.0; cameraPos.z = 0.0;
			rotation = glm::quat(-1, 0, 0, 0);
			tick = 0;
		}

	}

	int b = 1;
	glutSwapBuffers();
}

void init()
{
	srand(time(0));
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_BLEND);
	glEnable(GL_NORMALIZE);

	// for alpha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	programColor = shaderLoader.CreateProgram("shaders/shader_color.vert", "shaders/shader_color.frag");
	programTexture = shaderLoader.CreateProgram("shaders/shader_tex.vert", "shaders/shader_tex.frag");
	programLightning = shaderLoader.CreateProgram("shaders/shader_lightning.vert", "shaders/shader_lightning.frag");

	programDepth = shaderLoader.CreateProgram("shaders/shader_depth.vert", "shaders/shader_depth.frag");
	programShadow = shaderLoader.CreateProgram("shaders/shader_shadow.vert", "shaders/shader_shadow.frag");
	programLightningShadow = shaderLoader.CreateProgram("shaders/shader_lightning_shadow.vert", "shaders/shader_lightning_shadow.frag");

	programShadowBRDF = shaderLoader.CreateProgram("shaders/shader_shadow_brdf.vert", "shaders/shader_shadow_brdf.frag");
	programLightningShadowBRDF = shaderLoader.CreateProgram("shaders/shader_lightning_shadow_brdf.vert", "shaders/shader_lightning_shadow_brdf.frag");

	sphereModel = obj::loadModelFromFile("models/sphere.obj");
	shipModel = obj::loadModelFromFile("models/spaceship.obj");

	textureBubble = Core::LoadTexture("textures/bubble.png");
	textureShip = Core::LoadTexture("textures/ship.png");
	textureShipGlow = Core::LoadTexture("textures/ship_glow.png");

	fishModel = obj::loadModelFromFile("models/fish.obj");
	textureFish = Core::LoadTexture("textures/fish_texture.png");

	textureBG = Core::LoadTexture("textures/bg_texture_new.png");
	textureGround = Core::LoadTexture("textures/bg_texture_new_depths.png");

	textureEarth = Core::LoadTexture("textures/earth.png");
	textureEarthNormal = Core::LoadTexture("textures/earth_normalmap.png");
	textureFishNormal = Core::LoadTexture("textures/fish_normalmap.png");

	programShadowNormal = shaderLoader.CreateProgram("shaders/shader_shadow_normal.vert", "shaders/shader_shadow_normal.frag");
	programLightningShadowNormal = shaderLoader.CreateProgram("shaders/shader_lightning_shadow_normal.vert", "shaders/shader_lightning_shadow_normal.frag");

	// these two aren't used and probably won't, and neither were they tested; probably safe to delete
	programTextureEM = shaderLoader.CreateProgram("shaders/shader_tex_em.vert", "shaders/shader_tex_em.frag");
	programLightningEM = shaderLoader.CreateProgram("shaders/shader_lightning_em.vert", "shaders/shader_lightning_em.frag");

	programSimpleEM = shaderLoader.CreateProgram("shaders/shader_simple_em.vert", "shaders/shader_simple_em.frag");

	programSkybox = shaderLoader.CreateProgram("shaders/shader_skybox.vert", "shaders/shader_skybox.frag");

	programShip = shaderLoader.CreateProgram("shaders/shader_ship.vert", "shaders/shader_ship.frag");

	skybox = Core::setupCubeMap("textures/xpos.png", "textures/xneg.png", "textures/ypos.png", "textures/yneg.png", "textures/zpos.png", "textures/zneg.png");
	textureAsteroid = Core::LoadTexture("textures/asteroid2.png");

	// vao + vbo init (for skybox)
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);

	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(cubeVertices), cubeVertices);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLuint vPosition = glGetAttribLocation(programSkybox, "vPosition");
	glEnableVertexAttribArray(vPosition);
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// tangent init for normal mapping
	// fixed for fish model
	std::vector<float> unitY = { 0.0, 1.0, 0.0 };
	std::vector<float> unitX = { 1.0, 0.0, 0.0 };

	glm::vec3 uY = { 0.0, -1.0, 0.0 };
	glm::vec3 uX = { -1.0, 0.0, 0.0 };

	glm::vec3 tang[1119];

	for (int i = 0; i < fishModel.normal.size(); i += 3)
	{

		glm::vec3 normal = { fishModel.normal[i + 0], fishModel.normal[i + 1], fishModel.normal[i + 2] };

		if (fishModel.normal[i + 1] < 0.99 && fishModel.normal[i + 1] > -0.99) tang[i] = glm::normalize(glm::cross(normal, uY));
		else tang[i] = glm::normalize(glm::cross(normal, uX));

		tangent[i + 0] = tang[i].x;
		tangent[i + 1] = tang[i].y;
		tangent[i + 2] = tang[i].z;

	}

	// shadow mapping init

	// generate Framebuffer
	FramebufferObject = 0;
	glGenFramebuffers(1, &FramebufferObject);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferObject);

	// generate depth texture
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	// filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// attach depth texture to frame buffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// spawn items
	int i;

	for (i = 0; i < 750; i++) {
		BubblePositions.push_back(glm::ballRand(165.0));
	}

	for (i = 0; i < 320; i++) {
		FishPositions.push_back(glm::ballRand(85.0));
		if (i % 50 == 0)
			FishSize.push_back(12.5);
		else if (i % 20 == 0)
			FishSize.push_back(2.5);
		else if (i % 35 == 0)
			FishSize.push_back(0.5);
		else
			FishSize.push_back(1);
	}

	// 3 swarms, each with 15 fishes
	glm::vec3 currentFishPosition, currentSwarmPosition;

	for (int j = 0; j < FishSwarmCount; j++) {
		currentSwarmPosition = glm::ballRand(85.0);
		for (int i = 0; i < FishInSwarmCount; i++) {
			currentFishPosition = glm::ballRand(15.0);
			currentFishPosition += currentSwarmPosition;
			FishSwarmPositions.push_back(currentFishPosition);
			FishSwarmVelocity.push_back(glm::vec3(0.2f, 0.0f, 0.0f));
		}
	}

	// 10 portals to the deep waters
	for (int i = 0; i < 10; i++)
	{
		glm::vec3 position = glm::ballRand(30.0f);
		float scale = glm::linearRand(0.5f, 5.0f);
		portals.push_back(glm::vec4(position, scale));
	}

	// init particle class
	particles = new Particles();
	particles_right = new Particles(false);
}

void movement() 
{

	// could be optimized
	// it really should
	float moveSpeed = 0.1f + speedBonus;
	float maxSpeed = 6.5f;
	float stoppingValue = 0.82f;
	bool not_moving = false;

	// velocity calculation
	if (w_pressed)
	{
		if (accelerate < maxSpeed)
			accelerate += moveSpeed;
	}
	else if (s_pressed)
	{
		if (accelerate > -maxSpeed)
			accelerate -= moveSpeed;
	}

	if (a_pressed)
	{
		if (strafe > -maxSpeed)
			strafe -= moveSpeed;
	}
	else if (d_pressed)
	{
		if (strafe < maxSpeed)
			strafe += moveSpeed;
	}

	// auto stopping + changing engine mode
	if (accelerate > 0.0f)
	{
		particles->changeRandomization(1);
		particles_right->changeRandomization(1);

		accelerate -= moveSpeed * stoppingValue;
		if (accelerate < 0.0f) accelerate = 0.0f;
	}
	else if (accelerate < 0.0f)
	{
		particles->changeRandomization(1);
		particles_right->changeRandomization(1);

		accelerate += moveSpeed * stoppingValue;
		if (accelerate > 0.0f) accelerate = 0.0f;
	}
	else 
	{
		not_moving = true;
		//particles->changeRandomization(2);
		//particles_right->changeRandomization(2);
	}

	if (strafe > 0.0f)
	{
		particles->changeRandomization(1);
		if (not_moving) particles_right->changeRandomization(2);

		strafe -= moveSpeed * stoppingValue;
		if (strafe < 0.0f) strafe = 0.0f;
	}
	else if (strafe < 0.0f)
	{
		particles_right->changeRandomization(1);
		if (not_moving) particles->changeRandomization(2);

		strafe += moveSpeed * stoppingValue;
		if (strafe > 0.0f) strafe = 0.0f;
	}
	else
	{
		if (not_moving)
		{
			particles->changeRandomization(2);
			particles_right->changeRandomization(2);
		}
	}

	//std::cout << accelerate << std::endl;
	// position change
	cameraPos += cameraDir * (accelerate * 0.2);
	cameraPos += cameraSide * (strafe * 0.2);

	checkBounds();

}

void keyboard(unsigned char key, int x, int y)
{
	// false on input
	tour = false;
	float angleSpeed = 0.1f;
	float moveSpeed = 0.5f + speedBonus;

	switch (key)
	{
		//basic movement
	case 'w': w_pressed = true; break;
	case 's': s_pressed = true; break;
	case 'a': a_pressed = true; break;
	case 'd': d_pressed = true; break;

		//alternate mouse mode
	case 'f': rotate = true; break;
	/*
	//old
		//basic movement
	case 'w': cameraPos += cameraDir * moveSpeed; break;
	case 's': cameraPos -= cameraDir * moveSpeed; break;
	case 'd': cameraPos += cameraSide * moveSpeed; break;
	case 'a': cameraPos -= cameraSide * moveSpeed; break;
		//ascend / descend
	case 'u': cameraPos.y += moveSpeed; break;
	case 'j': cameraPos.y -= moveSpeed; break;
		//strafe
	case 'k': cameraPos.x += cameraSide.x * moveSpeed; cameraPos.z += cameraSide.z * moveSpeed; break;
	case 'h': cameraPos.x -= cameraSide.x * moveSpeed; cameraPos.z -= cameraSide.z * moveSpeed; break;
	*/

		//change camera
	case '1': changeCamera(); break;
	case '2': rotate = !rotate; break;
	//case '2': std::cout << glm::dot(cameraUp, glm::vec3(0,1,0)) << " " << std::endl;; break;
	case '3': cameraPos = glm::vec3(0.0f, 0.0f, 0.0f); break;
		//normalize camera
	case 'q': rotation = glm::quat(-1, 0, 0, 0); break;
		//for more speed
	case 'e': speedBonus += 0.05f; break;
	case 'r': if (speedBonus >= 0.05f) speedBonus -= 0.05f; break;
		//predefined tour
	case 'h': rotation = glm::quat(-1, 0, 0, 0); goBack(); cameraPos.x = -45; tour = true; break;
	case 'g': discoMode = !discoMode; break;
		//flashlight
	case 'x': flashlight = !flashlight; break;
	case 'c': flashlightColor = !flashlightColor; break;
	case 'v': randomizeFlashLightColor(); break;
		//testing
	case 'n': changeLevel(0); break;
	case 'm': changeLevel(1); break;
	case '4': goToSky(); break;

		//particles
	case '5': particles->changeRandomization(); 
		 	  particles_right->changeRandomization(); break;
	case '6': particles->changeTexture(1);
			  particles_right->changeTexture(1); break;
	case '7': particles->changeTexture(2);
			  particles_right->changeTexture(2); break;
	case '8': particles->changeTexture(3);
			  particles_right->changeTexture(3); break;
	case '9': particles->changeTexture(4);
			  particles_right->changeTexture(4); break;

		//quit
	case 'z': glutLeaveMainLoop(); break;
	}

}

void keyboardUp(unsigned char key, int x, int y)
{

	switch (key)
	{
		//basic movement - key release
	case 'w': w_pressed = false; break;
	case 's': s_pressed = false; break;
	case 'a': a_pressed = false; break;
	case 'd': d_pressed = false; break;
		//alternate mouse mode
	case 'f': rotate = false; break;
	}
	
}

void mouse(int x, int y)
{
	if (firstMouse)
	{
		lastY = y;
		lastX = x;
		firstMouse = false;
	}

	cameraY = y - lastY;
	cameraX = x - lastX;
	lastY = y;
	lastX = x;

	float sensitivity = 0.2f;
	cameraY *= sensitivity;
	cameraX *= sensitivity;

	// don't allow mouse to go outside window
	// unless mouse moves fast enough :)
	if (x > SCREEN_WIDTH - 200 || y < 200 || y > SCREEN_HEIGHT - 200 || x < 200) {
		glutWarpPointer(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
		lastX = SCREEN_WIDTH / 2; lastY = SCREEN_HEIGHT / 2;
		x = SCREEN_WIDTH / 2; y = SCREEN_HEIGHT / 2;
		cameraY = 0; cameraX = 0;
	}
}

void shutdown()
{
	shaderLoader.DeleteProgram(programColor);
	shaderLoader.DeleteProgram(programTexture);
	shaderLoader.DeleteProgram(programLightning);

	shaderLoader.DeleteProgram(programShadowNormal);
	shaderLoader.DeleteProgram(programLightningShadowNormal);
	shaderLoader.DeleteProgram(programTextureEM);
	shaderLoader.DeleteProgram(programLightningEM);
	shaderLoader.DeleteProgram(programDepth);
	shaderLoader.DeleteProgram(programShadow);
	shaderLoader.DeleteProgram(programLightningShadow);
	shaderLoader.DeleteProgram(programShadowBRDF);
	shaderLoader.DeleteProgram(programLightningShadowBRDF);
	shaderLoader.DeleteProgram(programSkybox);
	shaderLoader.DeleteProgram(programSimpleEM);
	shaderLoader.DeleteProgram(programShip);
}

void idle()
{
	glutPostRedisplay();
}

int main(int argc, char ** argv)
{
	glutInit(&argc, argv);
	glutSetOption(GLUT_MULTISAMPLE, 8);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE);
	glutInitWindowPosition(20, 20);
	glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutCreateWindow("OpenGL Projekt II");

	if (FULLSCREEN == true)
		glutFullScreen();

	glewInit();

	init();
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	glutPassiveMotionFunc(mouse);
	glutDisplayFunc(renderScene);
	glutIdleFunc(idle);

	// disable cursor
	glutSetCursor(GLUT_CURSOR_NONE);

	// main loop
	glutMainLoop();

	shutdown();

	return 0;
}
