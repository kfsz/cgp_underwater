#include "glew.h"
#include "freeglut.h"
#include "glm.hpp"
#include "ext.hpp"
#include "math.h"
#include <iostream>
#include <vector>
#include <windows.h>

#include "Shader_Loader.h"
#include "Render_Utils.h"
#include "Camera.h"
#include "Texture.h"

#include "common.h"

void drawObjectColor(obj::Model * model, glm::mat4 modelMatrix, glm::vec3 color);

void drawObjectTexture(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, bool transparency = false);

void createDepth(obj::Model * model, glm::mat4 modelMatrix);

void drawObjectShadow(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, bool transparency = false);

void drawObjectShadowBRDF(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, float metallic, float roughness, bool transparency = false, bool skipFlashlight = false);

void drawObjectShadowNormal(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, GLuint textureNormalId, bool transparency = false);

void drawObjectTextureEM(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, bool transparency, GLuint textureSkybox);

void drawSkybox(glm::mat4 modelMatrix, GLuint skyboxId);

void drawSimpleEM(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId);

// legacy
void drawPlayer();
