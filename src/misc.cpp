#include "misc.h"

void changeLevel(bool direction) {

	// only if in water
	if (sky)
		return;

	// check if only ground needs chaning
	if (direction == 1 && Level.ground == true) {
		Level.ground = false;
		return;
	}

	if (direction == 0 && Level.lvl > 0) {
		//go deeper
		Level.backgroundColorModifier -= 0.15f;
		Level.lightPower -= 0.35f;
		Level.lvl--;
	}
	// check for special underground level
	else if (direction == 0)
		Level.ground = true;

	if (direction == 1 && Level.lvl < 12) {
		//go up / add more
		Level.backgroundColorModifier += 0.15f;
		Level.lightPower += 0.35f;
		Level.lvl++;
	}

	// change background color (only once)
	backgroundColor = defBackgroundColor * Level.backgroundColorModifier;
}

void changeCamera() {

	if (firstPerson) {
	//	cameraPos += shipPos;
		shipPos = glm::vec3(-0.075f, -0.25f, -0.5f);
		cameraPos -= cameraDir * 0.5f;
		cameraPos -= cameraSide * 0.02f;
	//	cameraPos -= shipPos;
		firstPerson = false;
	}
	else {
	//	cameraPos += shipPos;
		shipPos = glm::vec3(0, -0.25f, 0.5f);
		cameraPos += cameraDir * 0.5f;
		cameraPos += cameraSide * 0.02f;
	//	cameraPos -= shipPos;
		firstPerson = true;
	}
}

void checkBounds()
{
	if (cameraPos.x < -100 || cameraPos.x > 100) goBack();
	else if (cameraPos.y < -100 || cameraPos.y > 100) {
		if (cameraPos.y < -100) changeLevel(0);
		else changeLevel(1);
		goBack();
	}
	else if (cameraPos.z < -100 || cameraPos.z > 100) goBack();
}

void checkBoundsNoChange(float max)
{
	if (cameraPos.x < -max || cameraPos.x > max) 
		goBack();
	else if (cameraPos.y < -max || cameraPos.y > max) 
		goBack();
	else if (cameraPos.z < -max || cameraPos.z > max) 
		goBack();
}

// collision with bubble
void kaboom()
{
	// color explosion!
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT); glutSwapBuffers();
	Sleep(150);
}

//shitty bounds check - seems like map is infinite -> it is!
void goBack()
{
	cameraPos.x = 0;
	cameraPos.y = 0;
	cameraPos.z = 0;

	//well, it werks
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT); glutSwapBuffers();
	Sleep(100);
}

void randomizeFlashLightColor() {
	flashLightColorRandom.x = rand() / (float)RAND_MAX;
	flashLightColorRandom.y = rand() / (float)RAND_MAX;
	flashLightColorRandom.z = rand() / (float)RAND_MAX;
}

glm::vec3 cohension(glm::vec3 currentFishPosition, int batch) {

	glm::vec3 FishCohension;
	for (int i = batch * FishInSwarmCount; i < (batch + 1) * FishInSwarmCount; i++) {
		FishCohension += FishSwarmPositions[i];
	}
	FishCohension = FishCohension / FishInSwarmCount;
	FishCohension = FishCohension - currentFishPosition;

	return FishCohension;
}

glm::vec3 separation(glm::vec3 currentFishPosition, glm::vec3 currentFishVelocity, int batch) {

	glm::vec3 currentFishVelocityB = glm::vec3(0.0f, 0.0f, 0.0f);

	for (int i = batch * FishInSwarmCount; i < (batch + 1) * FishInSwarmCount; i++) {
		if (glm::distance(FishSwarmPositions[i], currentFishPosition) < 12.0f) {
			currentFishVelocityB -= currentFishVelocity - glm::abs(FishSwarmPositions[i] - currentFishPosition);
		}
	}
	return currentFishVelocityB;
}

glm::vec3 alignment(glm::vec3 currentFishVelocity, int batch) {

	glm::vec3 FishAlingment;

	for (int i = batch * FishInSwarmCount; i < (batch + 1) * FishInSwarmCount; i++) {
		FishAlingment += FishSwarmVelocity[i];
	}
	FishAlingment = FishAlingment / FishSwarmVelocity.size();
	FishAlingment = FishAlingment - currentFishVelocity;

	return FishAlingment;
}

void goToSky() {

	// warp simulation
	kaboom();

	// return to water
	if (sky) {

		// return original light direction
		lightDir = glm::normalize(glm::vec3(0.4f, -1.0f, 0.005f));
		sky = false;

	}
	else {

		// only if on latest level
		// in preparation for portal
		
		// could have been done better but whatever
		while (Level.lvl < 9)
			changeLevel(1);
		while (Level.lvl > 9)
			changeLevel(0);

		if (Level.lvl == 9) {

			// change light direction due to skybox having sun in difrent place
			// can be further refined
			lightDir = glm::normalize(glm::vec3(-0.4f, -1.0f, 0.05f));

			sky = true;
		}
		glutSwapBuffers();
	}

}
