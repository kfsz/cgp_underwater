#include "draw.h"

void drawObjectColor(obj::Model * model, glm::mat4 modelMatrix, glm::vec3 color)
{
	GLuint program = programColor;

	glUseProgram(program);

	glUniform1f(glGetUniformLocation(program, "power"), Level.lightPower);
	
	glUniform3f(glGetUniformLocation(program, "objectColor"), color.x, color.y, color.z);
	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void drawObjectTexture(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, bool transparency)
{
	GLuint program;

	if (flashlight == 1)
		program = programLightning;
	else
		program = programTexture;

	glUseProgram(program);

	glUniform1f(glGetUniformLocation(program, "power"), Level.lightPower);

	if (transparency == false)
		glUniform1f(glGetUniformLocation(program, "transPower"), 1.0f);
	else
		glUniform1f(glGetUniformLocation(program, "transPower"), 0.15f); // 0.3 is also fine

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);

	Core::SetActiveTexture(textureId, "textureSampler", program, 0);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;

	float xx, yy, zz = 1.0f;
	if (discoMode == 1) {
		flashlight = 1;
		xx = round((std::rand() % 10) + 1) / 10;
		yy = round((std::rand() % 10) + 1) / 10;
		zz = round((std::rand() % 10) + 1) / 10;
	}

	if (flashlight == 1) {

		glUniform3f(glGetUniformLocation(program, "lightPosition"), cameraPos.x + shipPosOffset.x, cameraPos.y + shipPosOffset.y, cameraPos.z + shipPosOffset.z);

		glUniform3f(glGetUniformLocation(program, "lightDirection"), cameraDir.x, cameraDir.y, cameraDir.z);
		glUniform1f(glGetUniformLocation(program, "lightCutOff"), glm::cos(glm::radians(30.5f)));
		glUniform1f(glGetUniformLocation(program, "lightOuterCutOff"), glm::cos(glm::radians(37.5f))); //was 34.5

		if (flashlightColor == 1)
			glUniform3f(glGetUniformLocation(program, "lightColor"), flashLightColorRandom.x, flashLightColorRandom.y, flashLightColorRandom.z);
		else
			if (discoMode == 1)
				glUniform3f(glGetUniformLocation(program, "lightColor"), xx, yy, zz);
			else
				glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void createDepth(obj::Model * model, glm::mat4 modelMatrix)
{
	GLuint program = programDepth;

	glUseProgram(program);

	glm::mat4 concatenatedMatrix = VPMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "lightMVP"), 1, GL_FALSE, (float*)&concatenatedMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void drawObjectShadow(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, bool transparency)
{
	GLuint program;

	if (flashlight == 1)
		program = programLightningShadow;
	else
		program = programShadow;

	glUseProgram(program);

	glm::mat4 concatenatedMatrix = VPMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "lightMVP"), 1, GL_FALSE, (float*)&concatenatedMatrix);

	glUniform3f(glGetUniformLocation(program, "cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform1f(glGetUniformLocation(program, "power"), Level.lightPower);

	if (transparency == false)
		glUniform1f(glGetUniformLocation(program, "transPower"), 1.0f);
	else
		glUniform1f(glGetUniformLocation(program, "transPower"), 0.15f); // 0.3 is also fine

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);

	Core::SetActiveTexture(textureId, "textureSampler", program, 0);
	Core::SetActiveTexture(depthTexture, "textureDepth", program, 1);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;

	float xx, yy, zz = 1.0f;
	if (discoMode == 1) {
		flashlight = 1;
		xx = round((std::rand() % 10) + 1) / 10;
		yy = round((std::rand() % 10) + 1) / 10;
		zz = round((std::rand() % 10) + 1) / 10;
	}

	if (flashlight == 1) {

		glUniform3f(glGetUniformLocation(program, "lightPosition"), cameraPos.x + shipPosOffset.x, cameraPos.y + shipPosOffset.y, cameraPos.z + shipPosOffset.z);

		glUniform3f(glGetUniformLocation(program, "lightDirection"), cameraDir.x, cameraDir.y, cameraDir.z);
		glUniform1f(glGetUniformLocation(program, "lightCutOff"), glm::cos(glm::radians(30.5f)));
		glUniform1f(glGetUniformLocation(program, "lightOuterCutOff"), glm::cos(glm::radians(37.5f))); //was 34.5

		if (flashlightColor == 1)
			glUniform3f(glGetUniformLocation(program, "lightColor"), flashLightColorRandom.x, flashLightColorRandom.y, flashLightColorRandom.z);
		else
			if (discoMode == 1)
				glUniform3f(glGetUniformLocation(program, "lightColor"), xx, yy, zz);
			else
				glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void drawObjectShadowBRDF(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, float metallic, float roughness, bool transparency, bool skipFlashlight)
{
	GLuint program;

	if (flashlight == 1 && !skipFlashlight)
		program = programLightningShadowBRDF;
	else
		program = programShadowBRDF;

	glUseProgram(program);

	glUniform1f(glGetUniformLocation(program, "metallic"), metallic);
	glUniform1f(glGetUniformLocation(program, "roughness"), roughness);

	glm::mat4 concatenatedMatrix = VPMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "lightMVP"), 1, GL_FALSE, (float*)&concatenatedMatrix);

	glUniform3f(glGetUniformLocation(program, "cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform1f(glGetUniformLocation(program, "power"), Level.lightPower);

	if (transparency == false)
		glUniform1f(glGetUniformLocation(program, "transPower"), 1.0f);
	else
		glUniform1f(glGetUniformLocation(program, "transPower"), 0.15f); // 0.3 is also fine

	glUniform3f(glGetUniformLocation(program, "lightPos"), lightPos.x, lightPos.y, lightPos.z);

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);

	Core::SetActiveTexture(textureId, "textureSampler", program, 0);
	Core::SetActiveTexture(depthTexture, "textureDepth", program, 1);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;

	float xx, yy, zz = 1.0f;
	if (discoMode == 1) {
		flashlight = 1;
		xx = round((std::rand() % 10) + 1) / 10;
		yy = round((std::rand() % 10) + 1) / 10;
		zz = round((std::rand() % 10) + 1) / 10;
	}

	if (flashlight == 1) {

		glUniform3f(glGetUniformLocation(program, "lightPosition"), cameraPos.x + shipPosOffset.x, cameraPos.y + shipPosOffset.y, cameraPos.z + shipPosOffset.z);

		glUniform3f(glGetUniformLocation(program, "lightDirection"), cameraDir.x, cameraDir.y, cameraDir.z);
		glUniform1f(glGetUniformLocation(program, "lightCutOff"), glm::cos(glm::radians(30.5f)));
		glUniform1f(glGetUniformLocation(program, "lightOuterCutOff"), glm::cos(glm::radians(37.5f))); //was 34.5

		if (flashlightColor == 1)
			glUniform3f(glGetUniformLocation(program, "lightColor"), flashLightColorRandom.x, flashLightColorRandom.y, flashLightColorRandom.z);
		else
			if (discoMode == 1)
				glUniform3f(glGetUniformLocation(program, "lightColor"), xx, yy, zz);
			else
				glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void drawObjectShadowNormal(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, GLuint textureNormalId, bool transparency)
{
	GLuint program;

	if (flashlight == 1)
		program = programLightningShadowNormal;
	// for easy normal map testing
	//program = programLightningShadow;
	else
		program = programShadowNormal;

	glUseProgram(program);

	glm::mat4 concatenatedMatrix = VPMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "lightMVP"), 1, GL_FALSE, (float*)&concatenatedMatrix);

	glUniform3f(glGetUniformLocation(program, "cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform1f(glGetUniformLocation(program, "power"), Level.lightPower);

	if (transparency == false)
		glUniform1f(glGetUniformLocation(program, "transPower"), 1.0f);
	else
		glUniform1f(glGetUniformLocation(program, "transPower"), 0.15f); // 0.3 is also fine

	glUniform3f(glGetUniformLocation(program, "lightPos"), lightPos.x, lightPos.y, lightPos.z);

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);

	Core::SetActiveTexture(textureId, "textureSampler", program, 0);
	Core::SetActiveTexture(depthTexture, "textureDepth", program, 1);
	Core::SetActiveTexture(textureNormalId, "normalMap", program, 2);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;

	float xx, yy, zz = 1.0f;
	if (discoMode == 1) {
		flashlight = 1;
		xx = round((std::rand() % 10) + 1) / 10;
		yy = round((std::rand() % 10) + 1) / 10;
		zz = round((std::rand() % 10) + 1) / 10;
	}

	if (flashlight == 1) {

		glUniform3f(glGetUniformLocation(program, "lightPosition"), cameraPos.x + shipPosOffset.x, cameraPos.y + shipPosOffset.y, cameraPos.z + shipPosOffset.z);

		glUniform3f(glGetUniformLocation(program, "lightDirection"), cameraDir.x, cameraDir.y, cameraDir.z);
		glUniform1f(glGetUniformLocation(program, "lightCutOff"), glm::cos(glm::radians(30.5f)));
		glUniform1f(glGetUniformLocation(program, "lightOuterCutOff"), glm::cos(glm::radians(37.5f))); //was 34.5

		if (flashlightColor == 1)
			glUniform3f(glGetUniformLocation(program, "lightColor"), flashLightColorRandom.x, flashLightColorRandom.y, flashLightColorRandom.z);
		else
			if (discoMode == 1)
				glUniform3f(glGetUniformLocation(program, "lightColor"), xx, yy, zz);
			else
				glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModelT(model, &tangent[0]);

	glUseProgram(0);
}

void drawObjectTextureEM(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId, bool transparency, GLuint textureSkybox)
{
	GLuint program;

	if (flashlight == 1)
		program = programLightningEM;
	else
		program = programTextureEM;

	glUseProgram(program);

	glUniform1f(glGetUniformLocation(program, "power"), Level.lightPower);

	if (transparency == false)
		glUniform1f(glGetUniformLocation(program, "transPower"), 1.0f);
	else
		glUniform1f(glGetUniformLocation(program, "transPower"), 0.15f); // 0.3 is also fine

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);

	glUniform3f(glGetUniformLocation(program, "cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	Core::SetActiveTexture(textureId, "textureSampler", program, 0);

	glUniform1i(glGetUniformLocation(program, "skyboxSampler"), textureSkybox);
	glActiveTexture(GL_TEXTURE0 + textureSkybox);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureSkybox);

	//only skybox, no fishes reflection
	//far too advanced though :#

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;

	float xx, yy, zz = 1.0f;
	if (discoMode == 1) {
		flashlight = 1;
		xx = round((std::rand() % 10) + 1) / 10;
		yy = round((std::rand() % 10) + 1) / 10;
		zz = round((std::rand() % 10) + 1) / 10;
	}

	if (flashlight == 1) {

		glUniform3f(glGetUniformLocation(program, "lightPosition"), cameraPos.x + shipPosOffset.x, cameraPos.y + shipPosOffset.y, cameraPos.z + shipPosOffset.z);

		glUniform3f(glGetUniformLocation(program, "lightDirection"), cameraDir.x, cameraDir.y, cameraDir.z);
		glUniform1f(glGetUniformLocation(program, "lightCutOff"), glm::cos(glm::radians(30.5f)));
		glUniform1f(glGetUniformLocation(program, "lightOuterCutOff"), glm::cos(glm::radians(37.5f))); //was 34.5

		if (flashlightColor == 1)
			glUniform3f(glGetUniformLocation(program, "lightColor"), flashLightColorRandom.x, flashLightColorRandom.y, flashLightColorRandom.z);
		else
			if (discoMode == 1)
				glUniform3f(glGetUniformLocation(program, "lightColor"), xx, yy, zz);
			else
				glUniform3f(glGetUniformLocation(program, "lightColor"), 1.0f, 1.0f, 1.0f);
	}

	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::DrawModel(model);

	glUseProgram(0);
}

void drawSkybox(glm::mat4 modelMatrix, GLuint skyboxId)
{
	GLuint program = programSkybox;

	glUseProgram(program);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	Core::VertexData vertexData;
	vertexData.NumActiveAttribs = 1;
	vertexData.Attribs[0].Pointer = cubeVertices;
	vertexData.Attribs[0].Size = 4;
	vertexData.NumVertices = 36;

	//binding skybox texture
	glUniform1i(glGetUniformLocation(program, "cubeSampler"), skyboxId);
	glActiveTexture(GL_TEXTURE0 + skyboxId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxId);

	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);

	//Core::DrawVertexArray(vertexData);

	glUseProgram(0);
	glBindVertexArray(0);
}

void drawSimpleEM(obj::Model * model, glm::mat4 modelMatrix, GLuint textureId)
{
	GLuint program = programSimpleEM;

	glUseProgram(program);

	glUniform3f(glGetUniformLocation(program, "lightDir"), lightDir.x, lightDir.y, lightDir.z);
	Core::SetActiveTexture(textureId, "textureSampler", program, 0);

	glm::mat4 transformation = perspectiveMatrix * cameraMatrix * modelMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "modelViewProjectionMatrix"), 1, GL_FALSE, (float*)&transformation);
	glUniformMatrix4fv(glGetUniformLocation(program, "modelMatrix"), 1, GL_FALSE, (float*)&modelMatrix);

	//reflection
	glUniform3f(glGetUniformLocation(program, "cameraPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform1i(glGetUniformLocation(program, "cubeSampler"), skybox);
	glActiveTexture(GL_TEXTURE0 + skybox);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);


	Core::DrawModel(model);

	glUseProgram(0);
}

// not used
// let's keep it for possible testing
void drawPlayer() {

	glm::mat4 shipInitialTransformation = glm::translate(shipPos) * glm::rotate(glm::radians(180.0f), glm::vec3(0, 1, 0)) * glm::scale(glm::vec3(0.25f));
	glm::mat4 shipModelMatrix = glm::translate(cameraPos + cameraDir * 0.5f) * glm::mat4_cast(glm::inverse(rotation)) * shipInitialTransformation;
	//drawObjectColor(&shipModel, shipModelMatrix, glm::vec3(0.6f));
	//drawObjectTexture(&shipModel, shipModelMatrix, textureShip);
	drawObjectTexture(&shipModel, shipModelMatrix, textureShip);
}
