#include "glew.h"
#include "freeglut.h"
#include "glm.hpp"
#include "ext.hpp"
#include "math.h"
#include <iostream>
#include <vector>
#include <windows.h>

#include "Shader_Loader.h"
#include "Render_Utils.h"
#include "Camera.h"
#include "Texture.h"

#include "common.h"

glm::mat4 createCameraMatrix();
