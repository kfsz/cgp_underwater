#include "camera_extension.h"

glm::mat4 createCameraMatrix() 
{
	//rotation math
	glm::quat rotationChange;

	if (!rotate)
		rotationChange = glm::angleAxis(glm::radians(cameraX), glm::vec3(0.f, 1.f, 0.f)) * glm::angleAxis(glm::radians(cameraY), glm::vec3(1.f, 0.f, 0.f));
	else
		rotationChange = glm::angleAxis(glm::radians(cameraX), glm::vec3(0.f, 0.f, 1.f)) * glm::angleAxis(glm::radians(cameraY), glm::vec3(1.f, 0.f, 0.f));

	rotation = glm::normalize(rotationChange * rotation);

	cameraDir = glm::inverse(rotation) * glm::vec3(0, 0, -1);
	cameraSide = glm::inverse(rotation) * glm::vec3(1, 0, 0);

	cameraUp = glm::inverse(rotation) * glm::vec3(0, 1, 0);
	// stop movement
	cameraY = 0; cameraX = 0;

	return Core::createViewMatrixQuat(cameraPos, rotation);
}
