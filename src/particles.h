#pragma once

#include "common.h"

struct Particle 
{

	glm::vec2 Position;
	glm::vec2 Size;
	GLfloat Rotate = 0.0f;

	glm::vec4 Color;
	GLfloat Life, MaxLife;

	Particle(glm::vec2 pos, glm::vec2 size, GLfloat rotate, glm::vec4 color, GLfloat life)
		: Position(pos), Size(size), Rotate(rotate), Color(color), Life(life), MaxLife(life) {}

};

class Particles
{

public:
	Particles(bool left = true, GLuint amount = 69);
	~Particles();

	void draw();
	void update();
	void addRandomParticle();
	void addParticle(glm::vec2 pos = glm::vec2(785, 755), glm::vec2 size = glm::vec2(16, 16), GLfloat rotate = 0.0f,
				 	 glm::vec4 color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f), GLfloat life = 150.0f);
	void changeRandomization(GLuint mode = 0);
	void changeTexture(GLuint number);

private:
	std::vector<Particle> particles;
	glm::vec2 defPos;
	GLuint amount;
	GLuint program, texture, quadVAO;
	GLuint texture_i, texture_ii, texture_iii, texture_iv;
	bool gauss = true;

	static const float MAX_RANDOM_POS_LOW, MAX_RANDOM_POS_HIGH;
	static const float MAX_RANDOM_POS_DEVIATION;
	static const float MAX_RANDOM_SIZE_LOW, MAX_RANDOM_SIZE_HIGH;
	static const float MAX_RANDOM_ROTATE;
	static const float MAX_RANDOM_LIFE;
};

