#include "particles.h"

/*
todo:
 - nothing
*/


// ((760, 755));
// (1000, 755));

const float Particles::MAX_RANDOM_POS_LOW = -50.0f;
const float Particles::MAX_RANDOM_POS_HIGH = 50.0f;

const float Particles::MAX_RANDOM_POS_DEVIATION = 3.9f;

const float Particles::MAX_RANDOM_SIZE_LOW = -20.0f;
const float Particles::MAX_RANDOM_SIZE_HIGH = 20.0f;

const float Particles::MAX_RANDOM_ROTATE = 360.0f;
const float Particles::MAX_RANDOM_LIFE = 270.0f;


Particles::Particles(bool left, GLuint nr_particles)
{
	program = shaderLoader.CreateProgram("shaders/shader_particle.vert", "shaders/shader_particle.frag");

	texture_i = Core::LoadTexture("textures/particle_ichi.png");
	texture_ii = Core::LoadTexture("textures/particle_ni.png");
	texture_iii = Core::LoadTexture("textures/particle_san.png");
	texture_iv = Core::LoadTexture("textures/particle_shi.png");

	texture = texture_iii;

	// Configure VAO/VBO
	GLuint VBO;
	GLfloat vertices[] = {
		// Pos      // Tex
		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,

		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f
	};

	glGenVertexArrays(1, &this->quadVAO);
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindVertexArray(this->quadVAO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	if (left)
		this->defPos = glm::vec2(760, 755);
	else
		this->defPos = glm::vec2(1005, 755);

	std::vector<Particle> particles;

	for (GLuint i = 0; i < nr_particles; i++)
		// add offsets and all that
		//this->addParticle(this->defPos);
		this->addRandomParticle();
}

Particles::~Particles()
{
	shaderLoader.DeleteProgram(program);
}

void Particles::draw()
{
	  
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(program);

	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(SCREEN_WIDTH), static_cast<GLfloat>(SCREEN_HEIGHT), 0.0f, -1.0f, 1.0f);
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, (float*)&projection);

	for (Particle particle : this->particles)
	{

		glm::vec2 position = particle.Position;
		glm::vec2 size = particle.Size;

		// change size depending on lifetime
		// and adjust position
		size += (particle.MaxLife - particle.Life) * 0.4f;
		position -= size / 2;

		GLfloat rotate = particle.Rotate;
		glm::vec4 color = glm::vec4(particle.Color);

		glm::mat4 model;
		model = glm::translate(model, glm::vec3(position, 0.0f));
		model = glm::translate(model, glm::vec3(0.5f * size.x, 0.5f * size.y, 0.0f));
		model = glm::rotate(model, rotate, glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::translate(model, glm::vec3(-0.5f * size.x, -0.5f * size.y, 0.0f));
		model = glm::scale(model, glm::vec3(size, 1.0f));

		GLfloat opacity = particle.Life / (particle.MaxLife - 10.0f);

		glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, (float*)&model);
		glUniform3f(glGetUniformLocation(program, "spriteColor"), color.x, color.y, color.z);

		glUniform1f(glGetUniformLocation(program, "opacity"), opacity);

		Core::SetActiveTexture(texture, "image", program, 0);
		// also add opacity later

		glBindVertexArray(this->quadVAO);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glBindVertexArray(0);

	}

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Particles::update()
{
	// probably update and draw should be in one function

	float sideRotation = glm::dot(cameraSide, glm::vec3(0, 1, 0));
	float upRotation = glm::dot(cameraUp, glm::vec3(0, 1, 0));

	float speed;
	if (!gauss)
	{
		speed = 1.9f;
	}
	else
	{
		speed = 1.2f;
	}

	for (auto& particle_current : particles) {
		particle_current.Life -= 2;

		//particle_current.Position.x += 1.5 * sideRotation;
		//particle_current.Position.y -= 1.5 * upRotation;

		particle_current.Position.x += speed * sideRotation;
		particle_current.Position.y -= speed * upRotation;

	}

	//return; // skip for now
	int new_particles = 0;

	// replace dead particles
	particles.erase(
		std::remove_if(particles.begin(), particles.end(), [&](Particle const & particle_current) {
			bool replace = particle_current.Life <= 0;
			if (replace)
				new_particles++;
			return replace;
		}),
		particles.end());

	for (GLuint i = 0; i < new_particles; i++) {
		//std::cout << new_particles << std::endl;
		//addParticle(defPos);
		this->addRandomParticle();
	}
}

void Particles::addRandomParticle()
{
	// two types of particle randomization
	glm::vec2 position;
	if (gauss)
		position = glm::vec2(glm::gaussRand(this->defPos.x, MAX_RANDOM_POS_DEVIATION), glm::gaussRand(this->defPos.y, MAX_RANDOM_POS_DEVIATION));
	else
		position = this->defPos + glm::vec2(glm::linearRand(MAX_RANDOM_POS_LOW, MAX_RANDOM_POS_HIGH), glm::linearRand(MAX_RANDOM_POS_LOW, MAX_RANDOM_POS_HIGH));

	// also add various textures

	this->particles.push_back(Particle(position,
									  glm::vec2(16, 16) + glm::linearRand(MAX_RANDOM_SIZE_LOW, MAX_RANDOM_SIZE_HIGH),
									  glm::linearRand(0.0f, MAX_RANDOM_ROTATE),
									  // color probably shouldn't be totally random
									  // maybe also make this selectable
									  //glm::vec4(1.0f, 0.0f, 0.0f, 1.0f),
									  glm::vec4(glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), 1.0f),
									  //glm::vec4(glm::linearRand(0.2f, 1.0f), glm::linearRand(0.0f, 0.6f), glm::linearRand(0.0f, 0.6f), 1.0f),
									  glm::linearRand(30.0f, MAX_RANDOM_LIFE)));

}

void Particles::addParticle(glm::vec2 pos, glm::vec2 size, GLfloat rotate, glm::vec4 color, GLfloat life)
{
	this->particles.push_back(Particle(pos, size, rotate, color, life));
}

void Particles::changeRandomization(GLuint mode)
{
	// gauss maybe should be as nitro?
	if (mode == 0)
		this->gauss = !this->gauss;

	else if (mode == 1)
		this->gauss = true;
	else if (mode == 2)
		this->gauss = false;
}

void Particles::changeTexture(GLuint number)
{
	switch (number) {
		case 1: texture = texture_i; break;
		case 2: texture = texture_ii; break;
		case 3: texture = texture_iii; break;
		case 4: texture = texture_iv; break;
	}
}
