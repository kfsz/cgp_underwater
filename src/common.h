#pragma once

#include "glew.h"
#include "freeglut.h"
#include "glm.hpp"
#include "ext.hpp"
#include "math.h"
#include <iostream>
#include <vector>
#include <windows.h>

#include "Shader_Loader.h"
#include "Render_Utils.h"
#include "Camera.h"
#include "Texture.h"

#include "common.h"

struct level {
	int lvl = 6;
	float backgroundColorModifier = 1.0f;
	float lightPower = 0.6;
	bool ground = false;
};

extern GLuint programColor;
extern GLuint programTexture;
extern GLuint programLightning;

extern Core::Shader_Loader shaderLoader;

extern obj::Model shipModel;
extern obj::Model sphereModel;
extern obj::Model fishModel;

extern glm::vec3 cameraPos;
extern glm::vec3 cameraDir;
extern glm::vec3 cameraSide;

extern bool rotate;
extern bool firstPerson;
extern glm::vec3 shipPos;

extern glm::vec4 defBackgroundColor;
extern glm::vec4 backgroundColor;

extern glm::vec3 flashLightColorRandom;

extern struct level Level;

extern float cameraAngle;

extern std::vector<glm::vec3> BubblePositions;
extern std::vector<glm::vec3> FishPositions;
extern std::vector<float> FishSize;

extern glm::mat4 cameraMatrix, perspectiveMatrix;

extern glm::vec3 lightDir;
extern glm::quat rotation;

extern GLuint textureBubble;
extern GLuint textureFish;
extern GLuint textureBG;
extern GLuint textureGround;
extern GLuint textureShip;

extern float speedBonus;
extern float fishVeritcalDirection;
extern unsigned long count;

extern float cameraY, cameraX, lastY, lastX;
extern bool firstMouse;
extern bool tour;
extern bool flashlight;
extern bool flashlightColor;
extern bool discoMode;

extern long tick;
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern bool FULLSCREEN;

// fish swarm
extern int FishSwarmCount;
extern int FishInSwarmCount;
extern std::vector<glm::vec3> FishSwarmPositions;
extern std::vector<glm::vec3> FishSwarmVelocity;

// shadow mapping
extern GLuint programShadow;
extern GLuint programDepth;
extern GLuint depthTexture;
extern GLuint FramebufferObject;
extern GLuint programLightningShadow;

// brdf
extern GLuint programShadowBRDF;
extern GLuint programLightningShadowBRDF;
extern glm::vec3 lightPos;

// normal mapping
extern GLuint programShadowNormal;
extern GLuint programLightningShadowNormal;
extern std::vector<float> tangent;
extern GLuint textureEarth;
extern GLuint textureEarthNormal;
extern GLuint textureFishNormal;

// em
extern GLuint programTextureEM;
extern GLuint programLightningEM;
extern GLuint programSimpleEM;

// fish swarm v2
extern glm::mat4 projectionMatrix;
extern glm::mat4 cameraMatrix2;
extern glm::mat4 VPMatrix;

// vba/vao model drawing
extern int Index;
extern GLuint vao;
extern unsigned int fishVAO, fishVBO, fishEBO;

// other
extern glm::vec3 shipPosOffset;
extern bool sky;
extern const float cubeVertices[];

// for particle movement calculation
extern glm::vec3 cameraUp;

// skybox
extern GLuint skybox;
extern GLuint cubemapTexture;
extern GLuint programSkybox;
