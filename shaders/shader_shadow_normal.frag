#version 430 core

uniform sampler2D textureSampler;
uniform sampler2D textureDepth;
uniform sampler2D normalMap;

uniform vec3 lightDir;
uniform vec3 cameraPos;

uniform float power;
uniform float transPower;

in vec3 interpNormal;
in vec3 interpPos;
in vec2 interpTexCoord;

in vec4 lightPosition;

in vec3 lightWorld;
in vec3 cameraWorld;
in vec3 interpPosWorld;

float calculateShadow(vec4 lightPosition, vec3 normal)
{
	vec3 divided = lightPosition.xyz / lightPosition.w;
	divided = divided * 0.5 + 0.5;
	
	float closestDepth = texture2D(textureDepth, divided.xy).r;
	float currentDepth = divided.z;

	//closestDepth is rendered properly; fishes outlines are visible

	float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
	float shadow = currentDepth > closestDepth + bias  ? 1.0 : 0.0;
	return shadow;
}

void main()
{
	vec2 modifiedTexCoord = vec2(interpTexCoord.x, 1.0 - interpTexCoord.y);
	vec3 color = texture2D(textureSampler, modifiedTexCoord).rgb;
	
	//normalmap
	vec3 normal = texture2D(normalMap, modifiedTexCoord).rgb;
	normal = (normalize(normal) - 0.5f) * 2.0f;

	float diffuse = max(dot(normal, -lightWorld), 0.0);

	vec3 toEye = normalize(cameraWorld - interpPosWorld);
	float specular = pow(max(dot(toEye, reflect(lightWorld, normal)), 0.0), 30.0);

	float shadow = calculateShadow(lightPosition, normal);

	// for easy checking
	//gl_FragColor = vec4(shadow, shadow, shadow, 0.0);
	gl_FragColor = vec4(max((color - shadow), 0.0) * diffuse * power, transPower);
}
