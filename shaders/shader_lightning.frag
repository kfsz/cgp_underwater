#version 430 core

uniform sampler2D textureSampler;
uniform vec3 lightDir;

uniform vec3 lightPosition;
uniform vec3 lightColor;

uniform vec3 lightDirection;
uniform float lightCutOff;
uniform float lightOuterCutOff;

uniform float power;
uniform float transPower;

in vec3 interpNormal;
in vec2 interpTexCoord;

in vec3 fragPos;

void main()
{
	vec2 modifiedTexCoord = vec2(interpTexCoord.x, 1.0 - interpTexCoord.y);
	vec3 color = texture2D(textureSampler, modifiedTexCoord).rgb;
	vec3 normal = normalize(interpNormal);
	float diffuse = max(dot(normal, -lightDir), 0.0);

    float distance = length(lightPosition - fragPos);

	/*
		todo:
		normal mapping, shadow mapping, enviromental mapping
	*/

	// flashlight power
	float lightPower = 2.9; // was 0.95, 1.25
	float attenuation = 1.0 / (0.75 + distance * 0.08);  

	// flashlight calculation
	float theta = dot(normalize(lightPosition - fragPos), normalize(-lightDirection));

  	// for soft edges
	float epsilon = (lightCutOff - lightOuterCutOff);
	float intensity = clamp((theta - lightOuterCutOff) / epsilon, 0.0, 1.0);    
	
	if(theta > lightOuterCutOff) {    

		attenuation *= intensity;
		vec3 result = vec3(color * diffuse * power);

		//so that there won't be any strange situations, when light non-lightness overpowers flashlight's light
		result = max(vec3(0, 0, 0), result) + 
					max(vec3(0, 0, 0), vec3(lightPower * color * lightColor * attenuation));   

		gl_FragColor = vec4(result, transPower);

	} else {

		vec3 result = vec3(color * diffuse * power);
		gl_FragColor = vec4(result, transPower);

	}

}

