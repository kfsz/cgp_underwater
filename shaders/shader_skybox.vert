#version 430 core

layout(location = 0) in vec3 vertexPosition;
//layout(location = 1) in vec2 vertexTexCoord;
//layout(location = 2) in vec3 vertexNormal;

in vec4 vPosition;

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelMatrix;

out vec3 vertexPositionB;

void main()
{
	vertexPositionB = vPosition.xyz;
	gl_Position = modelViewProjectionMatrix * vec4(vPosition.xyz, 1.0);
}
