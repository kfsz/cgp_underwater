#version 430 core

uniform sampler2D textureSampler;
uniform samplerCube skyboxSampler;

uniform vec3 lightDir;

uniform float power;
uniform float transPower;

uniform vec3 cameraPos;

in vec3 interpNormal;
in vec2 interpTexCoord;

in vec3 vertexPositionC;

float fresnelSchlick(float cosTheta, float F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

void main()
{
	vec2 modifiedTexCoord = vec2(interpTexCoord.x, 1.0 - interpTexCoord.y);
	vec3 color = texture2D(textureSampler, modifiedTexCoord).rgb;
	vec3 normal = normalize(interpNormal);
	float diffuse = max(dot(normal, -lightDir), 0.0);

	//em
	vec3 viewDir = normalize(vertexPositionC -cameraPos);
	vec3 reflection = reflect(viewDir, normal);

	float opacity = 1.0f;
	opacity = max(0.0f, dot(-viewDir, normal));

	//reflaction
	float eta = 1.00f/1.33f;
	vec3 refraction = -refract(viewDir, normal, eta);

	//chromatic aberration
	vec3 refractionRed = -refract(viewDir, normal, eta - 0.02f);
	vec3 refractionGreen = -refract(viewDir, normal, eta + 0.02f);
	vec3 refractionBlue = -refract(viewDir, normal, eta - 0.01f);

	refraction = vec3(texture(skyboxSampler, refractionRed).r, texture(skyboxSampler, refractionGreen).g, texture(skyboxSampler, refractionBlue).b);

	//combine
	float F0 = (((1-eta)*(1-eta))/((1+eta)*(1+eta)));
	float cosTheta = max(0.0f, dot(lightDir, normal));
	float Schlick = fresnelSchlick(cosTheta, F0);
	vec3 colorMix = mix(refraction.rgb, texture(skyboxSampler, reflection).rgb, vec3(Schlick, Schlick, Schlick));

	gl_FragColor = vec4(colorMix * diffuse * power, transPower);
	//gl_FragColor = vec4(color * diffuse * power, transPower);
}
