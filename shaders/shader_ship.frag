#version 430 core

uniform sampler2D textureSampler;
uniform sampler2D textureDepth;
uniform sampler2D textureGlow;

uniform vec3 lightDir;
uniform vec3 lightPos;
uniform vec3 cameraPos;

uniform float power;
uniform float transPower;

in vec3 interpNormal;
in vec3 interpPos;
in vec2 interpTexCoord;

in vec4 lightPosition;

// material parameters
uniform float metallic;
uniform float roughness;
vec3 albedo;

float ao;

vec3 lightColors;

const float PI = 3.14159265359;
  
float Distribution(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float GeometrySchlickS1(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickS1(NdotV, roughness);
    float ggx1  = GeometrySchlickS1(NdotL, roughness);
	
    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

float calculateShadow(vec4 lightPosition, vec3 normal)
{
	vec3 divided = lightPosition.xyz / lightPosition.w;
	divided = divided * 0.5 + 0.5;
	
	float closestDepth = texture2D(textureDepth, divided.xy).r;
	float currentDepth = divided.z;

	//closestDepth is rendered properly; fishes outlines are visible

	float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
	float shadow = currentDepth > closestDepth + bias  ? 1.0 : 0.0;
	return shadow;
}

void main()
{
	vec2 modifiedTexCoord = vec2(interpTexCoord.x, 1.0 - interpTexCoord.y);
	vec3 colorTexture = texture2D(textureSampler, modifiedTexCoord).rgb;
	vec3 colorGlow = texture2D(textureGlow, modifiedTexCoord).rgb;

	vec3 normal = normalize(interpNormal);
	float diffuse = max(dot(normal, -lightDir), 0.0);
	
	// brdf
	lightColors = vec3(300.0);

	albedo = vec3(0.0f, 0.3f, 0.5f);
	ao = 1.0;
	vec3 N = normalize(interpNormal);
    vec3 V = normalize(cameraPos - interpPos);
	vec3 F0 = vec3(colorTexture);

	// brdf mixing is kinda wrong
	// calculate F0 value taking material properties into account
    //F0 = mix(F0, albedo, metallic);
	F0 = mix(F0, F0, metallic);
	//F0 = mix(F0, F0, F0);

    // calculate radiance
    vec3 L = normalize(lightPos - interpPos);
    vec3 H = normalize(V + L);
    float distance    = length(lightPos - interpPos) * 0.1f;
    float attenuation = 1.0 / (distance * distance);
    vec3 radiance     = lightColors * attenuation;  
	
    float cosTheta = max(0.0f, dot(normalize(L), normalize(N)));

	vec3 kD = vec3(1.0) - fresnelSchlick(cosTheta, F0);
	kD *= 1.0 - metallic;

	vec3 specular = fresnelSchlick(cosTheta, F0) * GeometrySmith(N, V, L, roughness) * Distribution(N, H, roughness);
    specular = specular / (4 * dot(N,L) * dot(N,V));

    // calculate outgoing radiance Lo
    float NdotL = max(dot(N, L), 0.0);      
	
    vec3 Lo =  4.0*(kD * albedo / PI + specular) * radiance * NdotL; 
  
    vec3 ambient = vec3(0.01) * albedo * ao;
    vec3 color = ambient + Lo;
	
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.0));  

	float shadow = calculateShadow(lightPosition, normal);

	// for easy checking
	//gl_FragColor = vec4(shadow, shadow, shadow, 1.0);

	if (power > 0.0)
		gl_FragColor = vec4(max((color - shadow), 0.0) * diffuse * power, transPower);
	else
	{
		vec3 colorNormal = max(vec3(0.0, 0.0, 0.0), vec3(colorTexture * (-1 * power)/10));
		colorGlow += colorNormal/5;
		gl_FragColor = vec4(colorGlow * (0 - power), transPower);	
	}

}
