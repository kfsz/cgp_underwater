#version 430 core

uniform sampler2D textureSampler;
uniform samplerCube skyboxSampler;

uniform vec3 lightDir;

uniform vec3 lightPosition;
uniform vec3 lightColor;

uniform vec3 lightDirection;
uniform float lightCutOff;
uniform float lightOuterCutOff;

uniform float power;
uniform float transPower;

uniform vec3 cameraPos;

in vec3 interpNormal;
in vec2 interpTexCoord;

in vec3 fragPos;

in vec3 vertexPositionC;

float fresnelSchlick(float cosTheta, float F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}  

void main()
{
	vec2 modifiedTexCoord = vec2(interpTexCoord.x, 1.0 - interpTexCoord.y);
	vec3 color = texture2D(textureSampler, modifiedTexCoord).rgb;
	vec3 normal = normalize(interpNormal);
	float diffuse = max(dot(normal, -lightDir), 0.0);

    float distance = length(lightPosition - fragPos);

	/*
		todo:
		normal mapping, shadow mapping, enviromental mapping
	*/

	//em
	vec3 viewDir = normalize(vertexPositionC -cameraPos);
	vec3 reflection = reflect(viewDir, normal);

	float opacity = 1.0f;
	opacity = max(0.0f, dot(-viewDir, normal));

	//reflaction
	float eta = 1.00f/1.33f;
	vec3 refraction = -refract(viewDir, normal, eta);

	//chromatic aberration
	vec3 refractionRed = -refract(viewDir, normal, eta - 0.02f);
	vec3 refractionGreen = -refract(viewDir, normal, eta + 0.02f);
	vec3 refractionBlue = -refract(viewDir, normal, eta - 0.01f);

	refraction = vec3(texture(skyboxSampler, refractionRed).r, texture(skyboxSampler, refractionGreen).g, texture(skyboxSampler, refractionBlue).b);

	//combine
	float F0 = (((1-eta)*(1-eta))/((1+eta)*(1+eta)));
	float cosTheta = max(0.0f, dot(lightDir, normal));
	float Schlick = fresnelSchlick(cosTheta, F0);
	vec3 colorMix = mix(refraction.rgb, texture(skyboxSampler, reflection).rgb, vec3(Schlick, Schlick, Schlick));

	//replace values
	color = colorMix;

	// flashlight power
	float lightPower = 2.9; // was 0.95, 1.25
	float attenuation = 1.0 / (0.75 + distance * 0.08);  

	// flashlight calculation
	float theta = dot(normalize(lightPosition - fragPos), normalize(-lightDirection));

  	// for soft edges
	float epsilon = (lightCutOff - lightOuterCutOff);
	float intensity = clamp((theta - lightOuterCutOff) / epsilon, 0.0, 1.0);    
	
	if(theta > lightOuterCutOff) {    

		attenuation *= intensity;
		vec3 result = vec3(color * diffuse * power);

		//so that there won't be any strange situations, when light non-lightness overpowers flashlight's light
		result = max(vec3(0, 0, 0), result) + 
					max(vec3(0, 0, 0), vec3(lightPower * color * lightColor * attenuation));   

		gl_FragColor = vec4(result, transPower);

	} else {

		vec3 result = vec3(color * diffuse * power);
		gl_FragColor = vec4(result, transPower);

	}

}

