#version 430 core

uniform sampler2D textureSampler;
uniform sampler2D textureDepth;
uniform sampler2D normalMap;

uniform vec3 lightDir;
uniform vec3 cameraPos;

uniform vec3 lightPosition;
uniform vec3 lightColor;

uniform vec3 lightDirection;
uniform float lightCutOff;
uniform float lightOuterCutOff;

uniform float power;
uniform float transPower;

in vec3 interpNormal;
in vec3 interpPos;
in vec2 interpTexCoord;

in vec3 fragPos;
in vec4 lightPositionShadow;

in vec3 lightWorld;
in vec3 cameraWorld;
in vec3 interpPosWorld;

float calculateShadow(vec4 lightPosition, vec3 normal)
{
	vec3 divided = lightPosition.xyz / lightPosition.w;
	divided = divided * 0.5 + 0.5;
	
	float closestDepth = texture2D(textureDepth, divided.xy).r;
	float currentDepth = divided.z;

	float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
	float shadow = currentDepth > closestDepth+bias  ? 1.0 : 0.0;
	return shadow;
}

void main()
{
	vec2 modifiedTexCoord = vec2(interpTexCoord.x, 1.0 - interpTexCoord.y);
	vec3 color = texture2D(textureSampler, modifiedTexCoord).rgb;
	//vec3 normal = normalize(interpNormal);
	//float diffuse = max(dot(normal, -lightDir), 0.0);

    float distance = length(lightPosition - fragPos);

	//normalmap
	vec3 normal = texture2D(normalMap, modifiedTexCoord).rgb;
	normal = (normalize(normal) - 0.5f) * 2.0f;
	
	float diffuse = max(dot(normal, -lightWorld), 0.0);

	vec3 toEye = normalize(cameraWorld - interpPosWorld);
	float specular = pow(max(dot(toEye, reflect(lightWorld, normal)), 0.0), 30.0);

	/*
		todo:
		normal mapping, !shadow mapping, enviromental mapping
	*/

	// flashlight power
	float lightPower = 2.9; // was 0.95, 1.25
	float attenuation = 1.0 / (0.75 + distance * 0.08);  

	// flashlight calculation
	float theta = dot(normalize(lightPosition - fragPos), normalize(-lightDirection));

  	// for soft edges
	float epsilon = (lightCutOff - lightOuterCutOff);
	float intensity = clamp((theta - lightOuterCutOff) / epsilon, 0.0, 1.0);    
	
	if(theta > lightOuterCutOff) {    

		attenuation *= intensity;
		vec3 result = vec3(color * diffuse * power);

		//so that there won't be any strange situations, when light non-lightness overpowers flashlight's light
		result = max(vec3(0, 0, 0), result) + 
					max(vec3(0, 0, 0), vec3(lightPower * color * lightColor * attenuation));   

		gl_FragColor = vec4(result, transPower);

	} else {

		// shadow mapping
		// only for places without flashlight
		// since nobody will see any diffrence anyways
		// fuck, nobody will see any diffrence anyways since shadows almost don't exist
		float shadow = calculateShadow(lightPositionShadow, normal);
		gl_FragColor = vec4(max((color - shadow), 0.0) * diffuse * power, transPower);

	}

}

