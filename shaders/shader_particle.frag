#version 430 core
in vec2 TexCoords;

uniform sampler2D image;
uniform vec3 spriteColor;
uniform float opacity;

void main()
{ 
	//vec4 color = vec4(spriteColor, 1.0) * texture(image, TexCoords);
	vec4 color = vec4(spriteColor, opacity) * texture(image, TexCoords);
	color.xyz *= 3;
	gl_FragColor = color;

}  
